import de.hu_berlin.keystore.Keystore;
import de.hu_berlin.keystore.KeystoreException;
import java.util.HexFormat;

public class KeystoreTest {

	public static void main(String[] args) {
		Keystore keystore;
		try {
			keystore = new Keystore();
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.SELECT_APPLET_FAILED) {
				System.out.println("Could not select the applet.");
			}
			if (e.getReason() == KeystoreException.CARD_NOT_PRESENT) {
				System.out.println("There is no card in the card reader.");
			}
			if (e.getReason() == KeystoreException.CARD_OPERATION_FAILED) {
				System.out.println("Card operation failed. Possibly missing a card reader?");
			}
			return;
		}
		try {
			keystore.initialize();
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.WRONG_PIN) {
				System.out.println("The entered pin is not correct.");
			}
			if (e.getReason() == KeystoreException.USING_CONTACTLESS_READER) {
				System.out.println("Contactless reader is not allowed for authentication.");
			}
			if (e.getReason() == KeystoreException.UNSUPPORTED_CARD_READER) {
				System.out.println("Cannot input the pin on this card reader.");
			}
			return;
		}
		try {
			keystore.installKey(HexFormat.of().parseHex("ca514602aa43cd61d6d2eb48a24d88ec"), "1102.0001.1");
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.WRONG_LENGTH) {
				System.out.println("The key must be 16 bytes long.");
			}
			if (e.getReason() == KeystoreException.KEY_NOT_INITIALIZED) {
				System.out.println("Wrapping key not initialized.");
			}
			return;
		}
		byte[] telegram = HexFormat.of().parseHex("2900BCE0110200010E03F11000273C45C9BF2B59E066BD0F");
		try {
			byte[] plaintext = keystore.decryptAndVerify2(telegram);
			if (plaintext[0] == 0 && plaintext[1] == (byte) 0x80) {
				System.out.println("success!");
			} else {
				System.out.println("FAILED!");
			}
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.MAC_VERIFICATION_FAILED) {
				System.out.println("The MAC is not valid!");
			}
			if (e.getReason() == KeystoreException.WRONG_LENGTH) {
				System.out.println("The telegram must be at least 23 bytes long.");
			}
			if (e.getReason() == KeystoreException.KEY_NOT_FOUND) {
				System.out.println("The key to decrypt this telegram cannot be found.");
			}
			return;
		}
		try {
			keystore.terminate();
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.WRONG_PIN) {
				System.out.println("The entered pin is not correct.");
			}
			if (e.getReason() == KeystoreException.USING_CONTACTLESS_READER) {
				System.out.println("Contactless reader is not allowed for authentication.");
			}
			if (e.getReason() == KeystoreException.UNSUPPORTED_CARD_READER) {
				System.out.println("Cannot input the pin on this card reader.");
			}
			return;
		}
		try {
			keystore.closeCardChannel();
		} catch (KeystoreException e) {
		}
	}
}
