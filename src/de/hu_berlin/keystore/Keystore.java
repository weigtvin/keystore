package de.hu_berlin.keystore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ClassNotFoundException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardNotPresentException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

public final class Keystore {

	/*
	 * CLASS CONSTANTS
	 */

	private static final int CLA = 0;

	private static final int INS_AUTHENTICATE = 0x20;
	private static final int INS_GENERATE_WRAPPING_KEY = 0x10;
	private static final int INS_REMOVE_WRAPPING_KEY = 0x30;
	private static final int INS_WRAP_KEY = 0x40;
	private static final int INS_UNWRAP_KEY = 0x50;
	private static final int INS_DECRYPT = 0xA0;
	private static final int INS_VERIFY = 0x80;
	private static final int INS_DECRYPT_AND_VERIFY = 0xB0;

	private static final int MAX_PIN_SIZE = 6;
	private static final int MIN_PIN_SIZE = 6;

	private static final String FILENAME = "keystore.dat";

	/*
	 * INSTANCE VARIABLES
	 */

	private Card card;
	private CardChannel channel;
	private Map<String, WrappedKnxKey> keyDict;

	/*
	 * ESTABLISHING AND CLOSING A CONNECTION TO THE SMARTCARD
	 */

	/**
	 * Constructs a new Keystore and establishes a connection to the Card.
	 *
	 * In order to use the methods of this class,
	 * a Keystore object must be created.
	 *
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.SELECT_APPLET_FAILED
	 * 	if the applet could not be found on the card
	 * 	<li>KeystoreException.CARD_NOT_PRESENT
	 * 	if a card reader was found, but no card
	 * 	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * </ul>
	 */
	public Keystore() throws KeystoreException {
		initializeCardChannel();
		selectApplet();
	}

	/**
	 * Closes the connection to the card.
	 *
	 * After calling this method,
	 * no other methods should be called on the object.
	 * Doing so may result in a runtime exception.
	 *
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * </ul>
	 */
	public void closeCardChannel() throws KeystoreException {
		try {
			card.disconnect(false);
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
	}

	/**
	 * Establishes a connection to the smartcard.
	 */
	private void initializeCardChannel() throws KeystoreException {
		try {
			TerminalFactory factory = TerminalFactory.getDefault();
			List<CardTerminal> terminals = factory.terminals().list();
			CardTerminal terminal = terminals.get(0);
			card = terminal.connect("*");
			channel = card.getBasicChannel();
		} catch (CardNotPresentException e) {
			throw new KeystoreException(KeystoreException.CARD_NOT_PRESENT);
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
	}

	/**
	 * Sends an APDU with SELECT-instruction to the Javacard-Applet.
	 */
	private void selectApplet() throws KeystoreException {
		int sw;
		try {
			byte[] appletAID = new byte[] {1, 2, 3, 4, 5, (byte) 0xA1};
			CommandAPDU apdu = new CommandAPDU(0, 0xA4, 4, 0, appletAID);
			sw = channel.transmit(apdu).getSW();
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		if (sw != 0x9000) {
			throw new KeystoreException(KeystoreException.SELECT_APPLET_FAILED);
		}
	}

	/*
	 * INITIALIZING AND TERMINATING THE KEYSTORE
	 */

	/**
	 * Initializes the keystore.
	 *
	 * If a dictionary exists on disk,
	 * it will be loaded and the wrapping key on the Javacard will not change.
	 * Otherwise,
	 * a new dictionary and a new wrapping key will be created.
	 *
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.WRONG_PIN
	 * 	if the entered pin is wrong
	 * 	<li>KeystoreException.USING_CONTACTLESS_READER
	 * 	if using a contactless reader
	 * 	<li>KeystoreException.UNSUPPORTED_CARD_READER
	 * 	if the reader does not support pin entry
	 * 	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * 	<li>KeystoreException.IO_ERROR
	 * 	if there was an error while reading the file
	 * </ul>
	 */
	public void initialize() throws KeystoreException {
		File file = new File(FILENAME);
		if (file.exists()) {
			keyDict = loadDictFromFile(file);
			return;
		}
		keyDict = new HashMap<>();
		insAuthenticate();
		insGenerateWrappingKey();
	}

	/**
	 * Deletes the dictionary from disk and removes the wrapping key from the Javacard.
	 *
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.WRONG_PIN
	 * 	if the entered pin is wrong
	 * 	<li>KeystoreException.USING_CONTACTLESS_READER
	 * 	if using a contactless reader
	 * 	<li>KeystoreException.UNSUPPORTED_CARD_READER
	 * 	if the reader does not support pin entry
	 * 	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * </ul>
	 */
	public void terminate() throws KeystoreException {
		keyDict.clear();
		new File(FILENAME).delete();
		insAuthenticate();
		insRemoveWrappingKey();
	}

	private Map<String, WrappedKnxKey> loadDictFromFile(File f) throws KeystoreException {
		try {
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			@SuppressWarnings("unchecked")
			Map<String, WrappedKnxKey> keyDict = (HashMap<String, WrappedKnxKey>) ois.readObject();
			ois.close();
			return keyDict;
		} catch (IOException | ClassNotFoundException e) {
			throw new KeystoreException(KeystoreException.IO_ERROR);
		}
	}

	private void insAuthenticate() throws KeystoreException {
		int sw;
		try {
			int controlCode = getVerifyPinControlCode();
			byte[] command = new PinVerifyStructure(MIN_PIN_SIZE, MAX_PIN_SIZE).getBytes();
			byte[] responseArray = card.transmitControlCommand(controlCode, command);
			sw = new ResponseAPDU(responseArray).getSW();
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		switch (sw) {
			case 0x6982:
				throw new KeystoreException(KeystoreException.WRONG_PIN);
			case 0x6983:
				throw new KeystoreException(KeystoreException.USING_CONTACTLESS_READER);
			case 0x6700:
				throw new KeystoreException(KeystoreException.WRONG_LENGTH);
		}
	}

	/**
	 * Returns the control code for the FEATURE_VERIFY_PIN_DIRECT.
	 */
	private int getVerifyPinControlCode() throws KeystoreException {
		byte b[] = getControlCodes();
		for (int i = 0; i < b.length; i += 6) {
			if (b[i] == 0x06) {
				ByteBuffer buf = ByteBuffer.wrap(b, i + 2, 4);
				return buf.getInt();
			}
		}
		throw new KeystoreException(KeystoreException.UNSUPPORTED_CARD_READER);
	}

	/**
	 * Returns all control codes that the current cardreader supports.
	 */
	private byte[] getControlCodes() throws KeystoreException {
		try {
			int controlCode = 0x42000d48;
			byte[] command = {};
			return card.transmitControlCommand(controlCode, command);
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
	}

	private void insGenerateWrappingKey() throws KeystoreException {
		int sw;
		try {
			CommandAPDU apdu = new CommandAPDU(CLA, INS_GENERATE_WRAPPING_KEY, 0, 0);
			sw = channel.transmit(apdu).getSW();
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		if (sw == 0x6985) {
			throw new KeystoreException(KeystoreException.NOT_AUTHENTICATED);
		}
	}

	private void insRemoveWrappingKey() throws KeystoreException {
		int sw;
		try {
			CommandAPDU apdu = new CommandAPDU(CLA, INS_REMOVE_WRAPPING_KEY, 0, 0);
			sw = channel.transmit(apdu).getSW();
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		if (sw == 0x6985) {
			throw new KeystoreException(KeystoreException.NOT_AUTHENTICATED);
		}
	}

	/*
	 * INSERT AND REMOVE KEYS INTO/FROM THE KEYSTORE
	 */

	/**
	 * Put a new wrapped KNX-Key into the dictionary.
	 *
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 *	<li>KeystoreException.WRONG_LENGTH;
	 *	if the provided key is not 16 bytes long
	 *	<li>KeystoreException.KEY_NOT_INITIALIZED;
	 *	if the wrapping key is not initialized
	 *	<li>KeystoreException.CARD_OPERATION_FAILED;
	 * 	if there was an error during transmission
	 * 	<li>KeystoreException.IO_ERROR
	 * 	if there was an error while writing the file
	 * </ul>
	 */
	public void installKey(byte[] key, String keyId) throws KeystoreException {
		keyDict.put(keyId, insWrapKey(new KnxKey(key)));
		writeDictToFile();
	}

	/**
	 * Removes a wrapped KNX-Key from the dictionary.
	 *
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.IO_ERROR
	 * 	if there was an error while writing the file
	 * </ul>
	 */
	public void removeKey(String keyId) throws KeystoreException {
		keyDict.remove(keyId);
		writeDictToFile();
	}

	private WrappedKnxKey insWrapKey(KnxKey k) throws KeystoreException {
		ResponseAPDU res;
		try {
			CommandAPDU apdu = new CommandAPDU(CLA, INS_WRAP_KEY, 0, 0, k.getBytes(), 32);
			res = channel.transmit(apdu);
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		switch (res.getSW()) {
			case 0x6700:
				throw new KeystoreException(KeystoreException.WRONG_LENGTH);
			case 0x6985:
				throw new KeystoreException(KeystoreException.KEY_NOT_INITIALIZED);
			case 0x6400:
				throw new KeystoreException(KeystoreException.EXECUTION_ERROR);
		}
		return new WrappedKnxKey(res.getData());
	}

	private void writeDictToFile() throws KeystoreException {
		try {
			FileOutputStream fos = new FileOutputStream(FILENAME);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(keyDict);
			oos.close();
		} catch (IOException e) {
			throw new KeystoreException(KeystoreException.IO_ERROR);
		}
	}

	/*
	 * DECRYPT AND VERIFY TELEGRAMS
	 */

	/**
	 * Verifies a KNX-Telegram.
	 *
	 * Uses only one instruction of the Javacard-Applet.
	 *
	 * @param telegram KNX-Telegram as a byte array
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.MAC_VERIFICATION_FAILED
	 * 	if the calculated mac differs from the mac in the telegram
	 * 	<li>KeystoreException.KEY_NOT_FOUND
	 * 	if the key to verify the telegram cannot be found in the dictionary
	 *	<li>KeystoreException.WRONG_LENGTH
	 *	if the telegram is not at least 23 bytes long
	 * 	<li>KeystoreException.KEY_NOT_INITIALIZED
	 *	if the wrapping key is not initialized
	 *	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * </ul>
	 */
	public void verify1(byte[] telegram) throws KeystoreException {
		KnxTelegram t = new KnxTelegram(telegram);
		insUnwrapKey(keyDict.get(t.getKeyId()));
		insDecryptAndVerify(t, true);
	}

	/**
	 * Verifies a KNX-Telegram.
	 *
	 * Uses two instructions of the Javacard-Applet.
	 *
	 * @param telegram KNX-Telegram as a byte array
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.MAC_VERIFICATION_FAILED
	 * 	if the calculated mac differs from the mac in the telegram
	 * 	<li>KeystoreException.KEY_NOT_FOUND
	 * 	if the key to verify the telegram cannot be found in the dictionary
	 *	<li>KeystoreException.WRONG_LENGTH
	 *	if the telegram is not at least 23 bytes long
	 * 	<li>KeystoreException.KEY_NOT_INITIALIZED
	 *	if the wrapping key is not initialized
	 *	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * </ul>
	 */
	public void verify2(byte[] telegram) throws KeystoreException {
		KnxTelegram t = new KnxTelegram(telegram);
		insUnwrapKey(keyDict.get(t.getKeyId()));
		byte[] plaintext = insDecrypt(t.buildIv(), t.getMac());
		t.setMac(plaintext);
		insVerify(t.getMac(), t.buildMessage());
	}

	/**
	 * Decrypts and verifies a KNX-Telegram.
	 *
	 * Uses only one instruction of the Javacard-Applet.
	 *
	 * @param telegram KNX-Telegram as a byte array
	 * @return decrypted payload of the telegram
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.MAC_VERIFICATION_FAILED
	 * 	if the calculated mac differs from the mac in the telegram
	 * 	<li>KeystoreException.KEY_NOT_FOUND
	 * 	if the key to decrypt and verify the telegram cannot be found in the dictionary
	 *	<li>KeystoreException.WRONG_LENGTH
	 *	if the telegram is not at least 23 bytes long
	 * 	<li>KeystoreException.KEY_NOT_INITIALIZED
	 *	if the wrapping key is not initialized
	 *	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * </ul>
	 */
	public byte[] decryptAndVerify1(byte[] telegram) throws KeystoreException {
		KnxTelegram t = new KnxTelegram(telegram);
		insUnwrapKey(keyDict.get(t.getKeyId()));
		return insDecryptAndVerify(t, false);
	}

	/**
	 * Decrypts and verifies a KNX-Telegram.
	 *
	 * Uses two instructions of the Javacard-Applet.
	 *
	 * @param telegram KNX-Telegram as a byte array
	 * @return decrypted payload of the telegram
	 * @throws KeystoreException with the following reason code:
	 * <ul>
	 * 	<li>KeystoreException.MAC_VERIFICATION_FAILED
	 * 	if the calculated mac differs from the mac in the telegram
	 * 	<li>KeystoreException.KEY_NOT_FOUND
	 * 	if the key to decrypt and verify the telegram cannot be found in the dictionary
	 *	<li>KeystoreException.WRONG_LENGTH
	 *	if the telegram is not at least 23 bytes long
	 * 	<li>KeystoreException.KEY_NOT_INITIALIZED
	 *	if the wrapping key is not initialized
	 *	<li>KeystoreException.CARD_OPERATION_FAILED
	 * 	if there was an error during transmission
	 * </ul>
	 */
	public byte[] decryptAndVerify2(byte[] telegram) throws KeystoreException {
		KnxTelegram t = new KnxTelegram(telegram);
		insUnwrapKey(keyDict.get(t.getKeyId()));
		byte[] plaintext = insDecrypt(t.buildIv(), t.getMacAndPayload());
		t.setMacAndPayload(plaintext);
		insVerify(t.getMac(), t.buildMessage());
		return t.getPayload();
	}

	private void insUnwrapKey(WrappedKnxKey w) throws KeystoreException {
		if (w == null) {
			throw new KeystoreException(KeystoreException.KEY_NOT_FOUND);
		}
		int sw;
		try {
			CommandAPDU apdu = new CommandAPDU(CLA, INS_UNWRAP_KEY, 0, 0, w.getBytes());
			sw = channel.transmit(apdu).getSW();
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		switch (sw) {
			case 0x6700:
				throw new KeystoreException(KeystoreException.WRONG_LENGTH);
			case 0x6985:
				throw new KeystoreException(KeystoreException.KEY_NOT_INITIALIZED);
			case 0x6400:
				throw new KeystoreException(KeystoreException.EXECUTION_ERROR);
			case 0x6982:
				throw new KeystoreException(KeystoreException.MAC_VERIFICATION_FAILED);
		}
	}

	private byte[] insDecrypt(byte[] iv, byte[] ciphertext) throws KeystoreException {
		ResponseAPDU res;
		try {
			byte[] data = concatenate(iv, ciphertext);
			CommandAPDU apdu = new CommandAPDU(CLA, INS_DECRYPT, 0, 0, data, ciphertext.length);
			res = channel.transmit(apdu);
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		switch (res.getSW()) {
			case 0x6700:
				throw new KeystoreException(KeystoreException.WRONG_LENGTH);
			case 0x6985:
				throw new KeystoreException(KeystoreException.KEY_NOT_INITIALIZED);
			case 0x6400:
				throw new KeystoreException(KeystoreException.EXECUTION_ERROR);
		}
		return res.getData();
	}

	private void insVerify(byte[] mac, byte[] message) throws KeystoreException {
		int sw;
		try {
			byte[] data = concatenate(mac, message);
			CommandAPDU apdu = new CommandAPDU(CLA, INS_VERIFY, 0, 0, data);
			sw = channel.transmit(apdu).getSW();
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		switch (sw) {
			case 0x6700:
				throw new KeystoreException(KeystoreException.WRONG_LENGTH);
			case 0x6985:
				throw new KeystoreException(KeystoreException.KEY_NOT_INITIALIZED);
			case 0x6400:
				throw new KeystoreException(KeystoreException.EXECUTION_ERROR);
			case 0x6982:
				throw new KeystoreException(KeystoreException.MAC_VERIFICATION_FAILED);
		}
	}

	/**
	 * Concatenates two byte arrays.
	 */
	private static byte[] concatenate(byte[] first, byte[] second) {
		byte[] res = new byte[first.length + second.length];
		System.arraycopy(first, 0, res, 0, first.length);
		System.arraycopy(second, 0, res, first.length, second.length);
		return res;
	}

	private byte[] insDecryptAndVerify(KnxTelegram t, boolean authOnly) throws KeystoreException {
		ResponseAPDU res;
		try {
			byte[] data = t.getBytes();
			int p1 = authOnly ? 1 : 0;
			int le = authOnly ? 0 : data.length - 22;
			CommandAPDU apdu = new CommandAPDU(CLA, INS_DECRYPT_AND_VERIFY, p1, 0, data, le);
			res = channel.transmit(apdu);
		} catch (CardException e) {
			throw new KeystoreException(KeystoreException.CARD_OPERATION_FAILED);
		}
		switch (res.getSW()) {
			case 0x6700:
				throw new KeystoreException(KeystoreException.WRONG_LENGTH);
			case 0x6985:
				throw new KeystoreException(KeystoreException.KEY_NOT_INITIALIZED);
			case 0x6400:
				throw new KeystoreException(KeystoreException.EXECUTION_ERROR);
			case 0x6982:
				throw new KeystoreException(KeystoreException.MAC_VERIFICATION_FAILED);
		}
		return res.getData();
	}

	/*
	 * MAIN FUNCTION
	 *
	 * This function can be used to do tests and benchmarks.
	 *
	 * Tests and benchmarks will always initialize and terminate the keystore.
	 * That means that a new wrapping key will be generated and deleted each time a test is run.
	 * That means that the pin has to be entered twice every time
	 * and the persistent memory of the smartcard will be altered.
	 * Also, tests with a contactless reader are not possible.
	 *
	 * To avoid this (e.g. during debugging),
	 * it is recommended to generate a wrapping key once on the smartcard
	 * and use this key for multiple tests.
	 * In order to do this,
	 * first run 'java de.hu_berlin.keystore.Keystore initialize'.
	 * Then, the following four lines of code have to be commented out.
	 *
	 * In the method initialize():
	 * insAuthenticate();
	 * insGenerateWrappingKey();
	 *
	 * In the method terminate():
	 * insAuthenticate();
	 * insRemoveWrappingKey();
	 *
	 * After recompilation, the following tests and benchmarks will use the persistent key on the smartcard.
	 */

	public static void main(String[] args) throws KeystoreException {
		if (args.length == 0) {
			printHelp();
			return;
		}
		switch (args[0]) {
			case "test":
				new Keystore().runTests();
				break;
			case "bench":
				if (args.length < 2) {
					printHelp();
					break;
				}
				new Keystore().runBenchmarks(Integer.parseInt(args[1]));
				break;
			case "initialize":
				new Keystore().initialize();
				break;
			case "help":
			case "--help":
			case "-h":
				printHelp();
				break;
			default:
				System.out.printf("Invalid argument '%s'\n", args[0]);
		}
	}

	private static void printHelp() {
		System.out.println("Usage: java de.hu_berlin.keystore.Keystore test");
		System.out.println("   or: java de.hu_berlin.keystore.Keystore bench NUM_TELEGRAMS");
		System.out.println("   or: java de.hu_berlin.keystore.Keystore initialize");
	}

	/*
	 * TESTS
	 */

	private void runTests() throws KeystoreException {
		String success = colorize("success", "green");
		String failed = colorize("FAILED", "red");
		String keyId = "1102.0001.1";

		initialize();
		installKey(HexFormat.of().parseHex("ca514602aa43cd61d6d2eb48a24d88ec"), keyId);
		System.out.printf("Running tests...\n\n");

		System.out.printf("Decryption and Verification with INS_DECRYPT_AND_VERIFY:\t");
		try {
			System.out.printf("%s\n", testDecrypt(true) ? success : failed);
		} catch (KeystoreException e) {
			System.out.println(failed);
		}

		System.out.printf("Decryption and Verification with INS_DECRYPT and INS_VERIFY:\t");
		try {
			System.out.printf("%s\n", testDecrypt(false) ? success : failed);
		} catch (KeystoreException e) {
			System.out.println(failed);
		}

		System.out.printf("Verification only with INS_DECRYPT_AND_VERIFY:\t\t\t");
		try {
			testVerify(true);
		} catch (KeystoreException e) {
			System.out.println(failed);
		}
		System.out.println(success);

		System.out.printf("Verification only with INS_DECRYPT and INS_VERIFY:\t\t");
		try {
			testVerify(false);
		} catch (KeystoreException e) {
			System.out.println(failed);
		}
		System.out.println(success);

		System.out.printf("Remove wrapped key from dictionary:\t\t\t\t");
		boolean wasRemoveKeySuccessful = false;
		removeKey(keyId);
		try {
			testDecrypt(true);
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.KEY_NOT_FOUND) {
				wasRemoveKeySuccessful = true;
			}
		}
		System.out.println(wasRemoveKeySuccessful ? success : failed);

		terminate();
		closeCardChannel();
	}

	private boolean testDecrypt(boolean useSingleInstruction) throws KeystoreException {
		byte[] telegram = HexFormat.of().parseHex("2900BCE0110200010E03F11000273C45C9BF2B59E066BD0F");
		byte[] plaintext;

		if (useSingleInstruction) {
			plaintext = decryptAndVerify1(telegram);
		} else {
			plaintext = decryptAndVerify2(telegram);
		}

		return (plaintext[0] == 0 && plaintext[1] == (byte) 0x80);
	}

	private void testVerify(boolean useSingleInstruction) throws KeystoreException {
		byte[] telegram = HexFormat.of().parseHex("2900BCE0110200010E03F11000273C45C9BF0080E066BD0F");

		if (useSingleInstruction) {
			verify1(telegram);
		} else {
			verify2(telegram);
		}
	}

	private static String colorize(String s, String color) {
		char c;
		switch (color) {
			case "red":   c = '1'; break;
			case "green": c = '2'; break;
			default:      return s;
		}
		char esc = (char) 27;
		String escapeSequenceColor = esc + "[3" + c + 'm';
		String escapeSequenceDefault = esc + "[0m";
		return escapeSequenceColor + s + escapeSequenceDefault;
	}

	private void printMap() {
		for (Map.Entry<String, WrappedKnxKey> entry : keyDict.entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}

	/*
	 * BENCHMARKS
	 */

	private void runBenchmarks(int numTelegrams) throws KeystoreException {
		double duration;

		initialize();
		installKey(HexFormat.of().parseHex("ca514602aa43cd61d6d2eb48a24d88ec"), "1102.0001.1");
		System.out.printf("Running benchmarks...\n\n");

		System.out.println("INS_DECRYPT and INS_VERIFY, always unwrap the key:");
		duration = benchmark2Unwrap(numTelegrams);
		printResults(numTelegrams, duration);
		System.out.println();

		System.out.println("INS_DECRYPT and INS_VERIFY, DO NOT unwrap the key:");
		duration = benchmark2NoUnwrap(numTelegrams);
		printResults(numTelegrams, duration);
		System.out.println();

		System.out.println("INS_DECRYPT and INS_VERIFY, DO NOT unwrap the key, DO NOT verify the MAC:");
		duration = benchmark2NoUnwrapNoVerify(numTelegrams);
		printResults(numTelegrams, duration);
		System.out.println();

		System.out.println("INS_DECRYPT_AND_VERIFY, always unwrap the key:");
		duration = benchmark1Unwrap(numTelegrams);
		printResults(numTelegrams, duration);
		System.out.println();

		System.out.println("INS_DECRYPT_AND_VERIFY, DO NOT unwrap the key:");
		duration = benchmark1NoUnwrap(numTelegrams);
		printResults(numTelegrams, duration);
		System.out.println();

		terminate();
		closeCardChannel();
	}

	private double benchmark2Unwrap(int numTelegrams) throws KeystoreException {
		byte[][] traffic = KnxSimulator.generateTraffic(numTelegrams);

		long start = System.currentTimeMillis();
		for (byte[] telegram : traffic) {
			decryptAndVerify2(telegram);
		}
		return (System.currentTimeMillis() - start) / 1000.0;
	}

	private double benchmark2NoUnwrap(int numTelegrams) throws KeystoreException {
		byte[][] traffic = KnxSimulator.generateTraffic(numTelegrams);
		insUnwrapKey(keyDict.get("1102.0001.1"));

		long start = System.currentTimeMillis();
		for (byte[] telegram : traffic) {
			KnxTelegram t = new KnxTelegram(telegram);
			byte[] plaintext = insDecrypt(t.buildIv(), t.getMacAndPayload());
			t.setMacAndPayload(plaintext);
			insVerify(t.getMac(), t.buildMessage());
		}
		return (System.currentTimeMillis() - start) / 1000.0;
	}

	private double benchmark2NoUnwrapNoVerify(int numTelegrams) throws KeystoreException {
		byte[][] traffic = KnxSimulator.generateTraffic(numTelegrams);
		insUnwrapKey(keyDict.get("1102.0001.1"));

		long start = System.currentTimeMillis();
		for (byte[] telegram : traffic) {
			KnxTelegram t = new KnxTelegram(telegram);
			insDecrypt(t.buildIv(), t.getMacAndPayload());
		}
		return (System.currentTimeMillis() - start) / 1000.0;
	}

	private double benchmark1Unwrap(int numTelegrams) throws KeystoreException {
		byte[][] traffic = KnxSimulator.generateTraffic(numTelegrams);

		long start = System.currentTimeMillis();
		for (byte[] telegram : traffic) {
			decryptAndVerify1(telegram);
		}
		return (System.currentTimeMillis() - start) / 1000.0;
	}

	private double benchmark1NoUnwrap(int numTelegrams) throws KeystoreException {
		byte[][] traffic = KnxSimulator.generateTraffic(numTelegrams);
		insUnwrapKey(keyDict.get("1102.0001.1"));

		long start = System.currentTimeMillis();
		for (byte[] telegram : traffic) {
			KnxTelegram t = new KnxTelegram(telegram);
			insDecryptAndVerify(t, false);
		}
		return (System.currentTimeMillis() - start) / 1000.0;
	}

	private static void printResults(int numTelegrams, double duration) {
		System.out.printf("number of telegrams:\t\t%d\n", numTelegrams);
		System.out.printf("elapsed time in seconds:\t%f\n", duration);
		System.out.printf("number of telegrams per second:\t%f\n", numTelegrams / duration);
	}
}
