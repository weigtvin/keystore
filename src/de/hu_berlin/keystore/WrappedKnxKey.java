package de.hu_berlin.keystore;

import java.io.Serializable;
import java.util.HexFormat;

final class WrappedKnxKey implements Serializable {

	private final byte[] bytes;

	/**
	 * Constructs a WrappedKnxKey from a byte array.
	 *
	 * @param b byte array
	 * @throws KeystoreException if the length of the byte array does not equal 32
	 */
	WrappedKnxKey(byte[] b) throws KeystoreException {
		if (b.length != 32) {
			throw new KeystoreException(KeystoreException.WRONG_LENGTH);
		}
		bytes = b;
	}

	/**
	 * Returns a byte array containing the WrappedKnxKey.
	 */
	byte[] getBytes() {
		return bytes;
	}

	/**
	 * Returns a string representation of the WrappedKnxKey.
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (byte b : bytes) {
			s.append(String.format("%02X", b));
		}
		return s.toString();
	}

	/**
	 * Main-function to test the methods of this class.
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("usage: java WrappedKnxKey HEXSTRING");
			return;
		}
		WrappedKnxKey key;
		try {
			key = new WrappedKnxKey(HexFormat.of().parseHex(args[0]));
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.WRONG_LENGTH) {
				System.out.println("key must be 32 bytes long");
			}
			return;
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			return;
		}
		System.out.println("toString():\t" + key);
		System.out.print("getBytes():\t");
		for (byte b : key.getBytes()) {
			System.out.printf("%02X", b);
		}
		System.out.println();
	}
}
