package de.hu_berlin.keystore;

public final class KeystoreException extends Exception {

	public static final short CARD_OPERATION_FAILED = 1;
	public static final short SELECT_APPLET_FAILED = 2;
	public static final short NOT_AUTHENTICATED = 3;
	public static final short UNSUPPORTED_CARD_READER = 4;
	public static final short WRONG_PIN = 5;
	public static final short USING_CONTACTLESS_READER = 6;
	public static final short WRONG_LENGTH = 7;
	public static final short IO_ERROR = 8;
	public static final short EXECUTION_ERROR = 9;
	public static final short KEY_NOT_INITIALIZED = 10;
	public static final short MAC_VERIFICATION_FAILED = 11;
	public static final short CARD_NOT_PRESENT = 12;
	public static final short KEY_NOT_FOUND = 13;

	final short reason;

	KeystoreException(short reason) {
		super();
		this.reason = reason;
	}

	public short getReason() {
		return reason;
	}
}
