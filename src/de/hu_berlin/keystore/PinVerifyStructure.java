package de.hu_berlin.keystore;

/**
 * Data that needs to be sent when calling SCardControl FEATURE_VERIFY_PIN_DIRECT.
 *
 * See pcsc specification part 10
 * (https://pcscworkgroup.com/Download/Specifications/pcsc10_v2.02.09.pdf)
 *
 * Detailed information about each structure element can be found in
 * USB Serial Bus Device Class Spec of USB Chip/Smart Card Interface Devices, Revision 1.1
 * (https://www.usb.org/sites/default/files/DWG_Smart-Card_CCID_Rev110.pdf)
 *
 * Structure elements that are longer than one byte are in little endian byte order.
 * For example, the German language identifier 0x0407 must be stored as 0x0704.
 * Equivalently, the ulDataLength is 0x0c000000 for length 12.
 *
 * This implementation creates a PinVerifyStructure so that the APDU,
 * that the card reader sends to the card applet,
 * has the following structure:
 *
 * CLA  INS  P1   P2   Lc                pinlen PIN_1 ... PIN_pinlen padding_1 ... padding_(maxPinLen-pinlen)
 * 0x00 0x20 0x00 0x80 (maxPinLen + 1) 0x??   0x??  ... 0x??       0xFF      ... 0xFF
 *
 * For example, assume maxPinLen == 6 and the PIN entered is 0258.
 * In this case, pinlen == 4.
 * The APDU will look as follows:
 * 0x00 0x20 0x00 0x80 0x07 0x04 0x00 0x02 0x05 0x08 0xFF 0xFF
 *
 * The instruction must be 0x20 (VERIFY instruction in ISO7816-4),
 * otherwise ReinerSCT cyberjack reader will refuse PIN entry and return statusword 0x6985.
 *
 * Example usage with javax.smartcardio library:
 *
 * private static Card card;
 * // initialize card here, see javax.smartcardio documentation
 * // select card applet here
 * int controlCode = 0x42000db2; // FEATURE_VERIFY_PIN_DIRECT on ReinerSCT cyberjack
 * byte[] command = new PinVerifyStructure(6, 6).getBytes();
 * byte[] response = card.transmitControlCommand(controlCode, command);
 *
 *
 * If you want to change this class,
 * e.g. because the APDU needs to be formatted differently,
 * then I highly recommend building a Java Card applet
 * that just echoes back every APDU it receives.
 * This way, one can see the APDU constructed by the card reader.
 */
final class PinVerifyStructure {
	private final byte bTimeOut;
	private final byte bTimeOut2;
	private final byte bmFormatString;
	private final byte bmPINBlockString;
	private final byte bmPINLengthFormat;
	private final byte maxPinLen;
	private final byte minPinLen;
	private final byte bEntryValidationCondition;
	private final byte bNumberMessage;
	private final short wLangId;
	private final byte bMsgIndex;
	private final byte bTeoPrologue[];
	private final byte ulDataLength[];
	private final byte abData[];

	/**
	 * Constructs a PinVerifyStructure.
	 *
	 * @param minPinLen minimum pin length
	 * @param mxPinLen maximum pin length
	 * @throws IllegalArgumentException if minPinLen is not positive, if minPinLen is greater than maxPinLen, and if maxPinLen is greater than 15
	 */
	PinVerifyStructure(int minPinLen, int maxPinLen) {
		if (minPinLen <= 0) {
			String msg = String.format("minimum pin length must be positive: %d", minPinLen);
			throw new IllegalArgumentException(msg);
		}
		if (minPinLen > maxPinLen) {
			String msg = String.format("maximum pin length must be greater or equal minimum pin length: %d, %d", minPinLen, maxPinLen);
			throw new IllegalArgumentException(msg);
		}
		if (maxPinLen > 0x0F) {
			String msg = String.format("maximum pin length must be smaller or equal 15: %d", maxPinLen);
			throw new IllegalArgumentException(msg);
		}

		/* Use default timeout. */
		this.bTimeOut = 0;

		/* Use default timeout after first key stroke. */
		this.bTimeOut2 = 0;

		/*
		 * We specify here, that we want the PIN to start at the second byte of the APDU data,
		 * because we want to reserve the first byte of the APDU data for the PIN length.
		 *
		 * Also, we left justify the PIN, and store it in binary format,
		 * i.e., one byte represents one digit of the PIN.
		 */
		this.bmFormatString = (byte)0b10001000;

		/*
		 * The first halfbyte describes the length in bits of the PIN length in the APDU.
		 * We set it here to 8 bits, so that we have one byte pin length information.
		 *
		 * The second halfbyte is the length in bytes of the PIN block in the APDU.
		 * We set it here to the maxPinLen,
		 * so that we can store the longest possible PIN,
		 * but do not waste extra bytes.
		 */
		this.bmPINBlockString = (byte)(0x80 | maxPinLen);

		/*
		 * Here, we set the position of the PIN length information to 0,
		 * so that the PIN length information begins at the first byte in the APDU data.
		 */
		this.bmPINLengthFormat = 0;

		/*
		 * These two bytes represent the 16 bit integer wPINMaxExtraDigit.
		 * The maxPinLen comes first,
		 * because the 16 bit integer is stored in little endian byte order.
		 */
		this.maxPinLen = (byte)maxPinLen;
		this.minPinLen = (byte)minPinLen;

		/* Accept PIN only after Validation key pressed on the reader pinpad. */
		this.bEntryValidationCondition = 2;

		/* Show the message indicated by bMsgIndex */
		this.bNumberMessage = 1;

		/*
		 * USB Language Identifiers:
		 * 0x0407 German (Standard)
		 * 0x0409 English (United States)
		 * (https://web.archive.org/web/20180829193331/http://www.usb.org/developers/docs/USB_LANGIDs.pdf/)
		 * Little endian, that means 0x0407 becomes 0x0704.
		 */
		this.wLangId = 0x0704;

		/* Which message to print when prompting for a PIN. */
		this.bMsgIndex = 0;

		/* not used */
		this.bTeoPrologue = new byte[] {0, 0, 0};

		/*
		 * APDU header (5 Bytes) + 1 Byte pinlen + maxPinLen
		 * Little endian integer represented by four bytes.
		 */
		this.ulDataLength = new byte[] {(byte)(5 + 1 + maxPinLen), 0, 0, 0};

		/* The APDU */
		this.abData = new byte[ulDataLength[0]];
		abData[0] = 0;
		abData[1] = 0x20; // must be 0x20
		abData[2] = 0;
		abData[3] = (byte)0x80;
		abData[4] = (byte)(maxPinLen + 1);
		abData[5] = 0;
		for (int i = 6; i < abData.length; ++i) {
			abData[i] = (byte)0xFF; // padding, will be filled with the PIN by the reader.
		}
	}

	/**
	 * Returns a byte array containing the PinVerifyStructure.
	 */
	byte[] getBytes() {
		byte b[] = new byte[19 + this.abData.length];
		b[0] = bTimeOut;
		b[1] = bTimeOut2;
		b[2] = bmFormatString;
		b[3] = bmPINBlockString;
		b[4] = bmPINLengthFormat;
		b[5] = maxPinLen;
		b[6] = minPinLen;
		b[7] = bEntryValidationCondition;
		b[8] = bNumberMessage;
		b[9] = (byte)(wLangId >> 8);
		b[10] = (byte)(wLangId & 0x00FF);
		b[11] = bMsgIndex;
		System.arraycopy(bTeoPrologue, 0, b, 12, bTeoPrologue.length);
		System.arraycopy(ulDataLength, 0, b, 15, ulDataLength.length);
		System.arraycopy(abData, 0, b, 19, abData.length);
		return b;
	}

	/**
	 * Returns a string representation of the PinVerifyStructure.
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(String.format("%02X ", bTimeOut));
		s.append(String.format("%02X ", bTimeOut2));
		s.append(String.format("%02X ", bmFormatString));
		s.append(String.format("%02X ", bmPINBlockString));
		s.append(String.format("%02X ", bmPINLengthFormat));
		s.append(String.format("%02X ", maxPinLen));
		s.append(String.format("%02X ", minPinLen));
		s.append(String.format("%02X ", bEntryValidationCondition));
		s.append(String.format("%02X ", bNumberMessage));
		s.append(String.format("%02X ", wLangId >> 8));
		s.append(String.format("%02X ", wLangId & 0x00FF));
		s.append(String.format("%02X ", bMsgIndex));
		for (int i = 0; i < bTeoPrologue.length; ++i) {
			s.append(String.format("%02X ", bTeoPrologue[i]));
		}
		for (int i = 0; i < ulDataLength.length; ++i) {
			s.append(String.format("%02X ", ulDataLength[i]));
		}
		for (int i = 0; i < abData.length; ++i) {
			s.append(String.format("%02X ", abData[i]));
		}
		return s.toString();
	}

	/**
	 * Main-function to test the methods of this class.
	 */
	public static void main(String args[]) {
		if (args.length != 2) {
			System.out.println("usage: java PinVerifyStructure MIN_PIN_LEN MAX_PIN_LEN");
			return;
		}
		int minPinLen = Integer.parseInt(args[0]);
		int maxPinLen = Integer.parseInt(args[1]);
		PinVerifyStructure s = new PinVerifyStructure(minPinLen, maxPinLen);
		System.out.print("toString():\t");
		System.out.println(s);
		System.out.print("getBytes():\t");
		for (byte b: s.getBytes()) {
			System.out.printf("%02X ", b);
		}
		System.out.println();
	}
}
