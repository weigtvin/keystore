package de.hu_berlin.keystore;

import java.security.AlgorithmParameters;
import java.security.Key;
import java.util.HexFormat;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class provides the function generateTraffic(),
 * to generate encrypted and authenticated KNX-Telegrams.
 */
final class KnxSimulator {

	/**
	 * Constructor is private to prevent clients from creating an object of type "KnxSimulator".
	 */
	private KnxSimulator() {}

	/**
	 * Encrypts the payload and the MAC of a KNX-Telegram.
	 *
	 * @param t KNX-Telegram with MAC
	 * @param k encryption key
	 * @throws KeystoreException if encryption failed
	 */
	private static void encryptTelegram(KnxTelegram t, KnxKey k) throws KeystoreException {
		try {
			Cipher ctr = Cipher.getInstance("AES/CTR/NoPadding");
			Key key = new SecretKeySpec(k.getBytes(), "AES");
			AlgorithmParameters params = AlgorithmParameters.getInstance("AES");
			params.init(new IvParameterSpec(t.buildIv()));
			ctr.init(Cipher.ENCRYPT_MODE, key, params);
			t.setMacAndPayload(ctr.doFinal(t.getMacAndPayload()));
		} catch (Exception e) {
			throw new KeystoreException(KeystoreException.EXECUTION_ERROR);
		}
	}

	/**
	 * Calculates a CBC-MAC of length 4 Bytes.
	 *
	 * @param msg message to calculate the MAC over
	 * @param k key to use for the MAC
	 * @return CBC-MAC of size 4 Bytes
	 * @throws KeystoreException if mac creation failed
	 */
	private static byte[] mac(byte[] msg, KnxKey k) throws KeystoreException {
		try {
			Cipher cbc = Cipher.getInstance("AES/CBC/NoPadding");
			Key key = new SecretKeySpec(k.getBytes(), "AES");
			AlgorithmParameters params = AlgorithmParameters.getInstance("AES");
			params.init(new IvParameterSpec(new byte[16]));
			cbc.init(Cipher.ENCRYPT_MODE, key, params);
			byte[] out = cbc.doFinal(msg);
			byte[] mac = new byte[4];
			System.arraycopy(out, out.length - 16, mac, 0, mac.length);
			return mac;
		} catch (Exception e) {
			throw new KeystoreException(KeystoreException.EXECUTION_ERROR);
		}
	}

	/**
	 * Creates a KNX-Telegram that is not encrypted and does not contain a MAC value.
	 *
	 * @param addr destination address
	 * @param len length of the payload
	 * @param seqNr sequence number
	 * @return unencrypted KNX-Telegram without MAC
	 */
	private static KnxTelegram buildTelegram(short addr, int len, long seqNr) {
		StringBuilder s = new StringBuilder();
		s.append("2900BCE01102");
		s.append(String.format("%04X", addr));
		s.append(String.format("%02X", 8 + len + 4));
		s.append("03F110");
		s.append(String.format("%012X", seqNr));
		for (int i = 1; i <= len; ++i) {
			s.append(String.format("%02X", i));
		}
		s.append("00000000");
		return new KnxTelegram(HexFormat.of().parseHex(s.toString()));
	}

	/**
	 * Creates an encrypted and authenticated KNX-Telegram.
	 *
	 * @param addr destination address
	 * @param key key used to calculate the MAC and encrypt the telegram
	 * @param len length of the payload
	 * @param seqNr sequence number
	 * @return encrypted and authenticated KNX-Telegram
	 * @throws KeystoreException if encryption or mac creation failed
	 */
	private static KnxTelegram sendTelegram(short addr, KnxKey key, int len, long seqNr) throws KeystoreException {
		KnxTelegram t = buildTelegram(addr, len, seqNr);
		byte[] msg = t.buildMessage();
		byte[] mac = mac(msg, key);
		t.setMac(mac);
		encryptTelegram(t, key);
		return t;
	}

	/**
	 * Converts an array of KnxTelegrams to an array of byte arrays.
	 *
	 * @param t array of KnxTelegrams
	 * @return array of byte arrays
	 */
	private static byte[][] knxTelegramsToByteArrays(KnxTelegram[] t) {
		byte[][] b = new byte[t.length][];
		for (int i = 0; i < t.length; ++i) {
			b[i] = t[i].getBytes();
		}
		return b;
	}

	/**
	 * Returns an array of KNX-Telegrams of size n.
	 *
	 * @param n number of KNX-Telegrams
	 * @return array of n encrypted and authenticated KNX-Telegrams
	 * @throws KeystoreException if creation of a Telegram failed
	 */
	static byte[][] generateTraffic(int n) throws KeystoreException {
		KnxTelegram[] traffic = new KnxTelegram[n];
		KnxKey key = new KnxKey(HexFormat.of().parseHex("ca514602aa43cd61d6d2eb48a24d88ec"));
		long seqNr = 0;
		for (int i = 0; i < n; ++i) {
			traffic[i] = sendTelegram((short) 1, key, 5, ++seqNr);
		}
		return knxTelegramsToByteArrays(traffic);
	}

	/**
	 * Takes a number as argument,
	 * creates this many KNX-Telegrams,
	 * and prints them.
	 */
	public static void main(String[] args) throws KeystoreException {
		if (args.length != 1) {
			System.out.println("usage: java KnxSimulator NUM_TELEGRAMS");
			return;
		}
		int n = Integer.parseInt(args[0]);
		for (byte[] telegram : generateTraffic(n)) {
			System.out.println(new KnxTelegram(telegram));
		}
	}
}
