package de.hu_berlin.keystore;

import java.util.HexFormat;

/**
 * This class describes the structure of a "KNX Data Secure"-Telegram,
 * and provides methods that are used by the Keystore.
 */
final class KnxTelegram {

	private final byte   messageCode;
	private final byte   addInfoLength;
	private final byte   controlField1;
	private final byte   controlField2;
	private final byte[] sourceAddress;
	private final byte[] destinationAddress;
	private final byte   payloadLength;
	private final byte[] tpciSecureApci;
	private final byte   scf;
	private final byte[] seqNr;
	private       byte[] payload;
	private       byte[] mac;

	/**
	 * Constructs a new KnxTelegram from a byte array.
	 *
	 * @param bytes telegram in byte array representation
	 * @throws KeystoreException if byte array is too short
	 */
	KnxTelegram(byte[] bytes) throws KeystoreException {
		if (bytes.length < 18 + 1 + 4) {
			throw new KeystoreException(KeystoreException.WRONG_LENGTH);
		}
		messageCode = bytes[0];
		addInfoLength = bytes[1];
		controlField1 = bytes[2];
		controlField2 = bytes[3];
		sourceAddress = new byte[] {bytes[4], bytes[5]};
		destinationAddress = new byte[] {bytes[6], bytes[7]};
		payloadLength = bytes[8];
		tpciSecureApci = new byte[] {bytes[9], bytes[10]};
		scf = bytes[11];
		seqNr = new byte[6];
		System.arraycopy(bytes, 12, seqNr, 0, seqNr.length);
		payload = new byte[bytes.length - 18 - 4];
		System.arraycopy(bytes, 18, payload, 0, payload.length);
		mac = new byte[4];
		System.arraycopy(bytes, bytes.length - mac.length, mac, 0, mac.length);
	}

	/**
	 * Returns the address type of the telegram.
	 *
	 * @return 0x80 if destination address is a group address, 0 else
	 */
	private byte getAddressType() {
		return (byte) (controlField2 & 0b1000_0000);
	}	

	/**
	 * Returns the Key-ID of the key that was used to encrypt the telegram.
	 *
	 * @return key-id: {sourceAddress}.{destinationAddress}.{addressType}
	 */
	String getKeyId() {
		StringBuilder s = new StringBuilder();
		for (byte b : sourceAddress) {
			s.append(String.format("%02x", b));
		}
		s.append('.');
		for (byte b : destinationAddress) {
			s.append(String.format("%02x", b));
		}
		s.append('.');
		s.append(getAddressType() != 0 ? '1' : '0');
		return s.toString();
	}

	/**
	 * Creates the message that is going to be authenticated.
	 *
	 * The MAC of a KNX-Telegram is not calculated over the telegram itself,
	 * but rather over a message that can be derived from the telegram.
	 * This method creates this message from the given telegram.
	 *
	 * @return message as a byte array
	 */
	byte[] buildMessage() {
		byte[] res = new byte[16 * (2 + (payload.length + 2) / 16)];
		// Block 0:
		System.arraycopy(seqNr, 0, res, 0, seqNr.length);
		System.arraycopy(sourceAddress, 0, res, 6, sourceAddress.length);
		System.arraycopy(destinationAddress, 0, res, 8, destinationAddress.length);
		res[11] = getAddressType();
		System.arraycopy(tpciSecureApci, 0, res, 12, tpciSecureApci.length);
		res[14] = 0;
		res[15] = (byte) (payload.length);
		// Block 1:
		res[16 + 0] = 0;
		res[16 + 1] = 1;
		res[16 + 2] = scf;
		System.arraycopy(payload, 0, res, 16 + 3, payload.length);
		return res;
	}

	/**
	 * Creates the initialization vector to encrypt the KNX-Telegram.
	 *
	 * @return initialization vector as a byte array
	 */
	byte[] buildIv() {
		byte[] res = new byte[16];
		System.arraycopy(seqNr, 0, res, 0, seqNr.length);
		System.arraycopy(sourceAddress, 0, res, 6, sourceAddress.length);
		System.arraycopy(destinationAddress, 0, res, 8, destinationAddress.length);
		res[10] = 0;
		res[11] = 0;
		res[12] = 0;
		res[13] = 0;
		res[14] = 1;
		return res;
	}

	byte[] getPayload() {
		return payload;
	}

	void setPayload(byte[] newPayload) {
		payload = newPayload;
	}

	byte[] getMac() {
		return mac;
	}

	void setMac(byte[] newMac) {
		mac = newMac;
	}

	/**
	 * Returns a byte array containing the MAC and payload.
	 */
	byte[] getMacAndPayload() {
		byte[] res = new byte[mac.length + payload.length];
		System.arraycopy(mac, 0, res, 0, mac.length);
		System.arraycopy(payload, 0, res, mac.length, payload.length);
		return res;
	}

	/**
	 * Sets MAC and payload from a byte array.
	 */
	void setMacAndPayload(byte[] data) {
		System.arraycopy(data, 0, mac, 0, mac.length);
		System.arraycopy(data, mac.length, payload, 0, payload.length);
	}

	/**
	 * Returns a byte array containing the KnxTelegram.
	 */
	byte[] getBytes() {
		byte b[] = new byte[18 + payload.length + 4];
		b[0] = messageCode;
		b[1] = addInfoLength;
		b[2] = controlField1;
		b[3] = controlField2;
		System.arraycopy(sourceAddress, 0, b, 4, sourceAddress.length);
		System.arraycopy(destinationAddress, 0, b, 6, destinationAddress.length);
		b[8] = payloadLength;
		System.arraycopy(tpciSecureApci, 0, b, 9, tpciSecureApci.length);
		b[11] = scf;
		System.arraycopy(seqNr, 0, b, 12, seqNr.length);
		System.arraycopy(payload, 0, b, 18, payload.length);
		System.arraycopy(mac, 0, b, b.length - mac.length, mac.length);
		return b;
	}

	/**
	 * Returns a string representation of the KnxTelegram.
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(String.format("%02X", messageCode));
		s.append(String.format("%02X", addInfoLength));
		s.append(String.format("%02X", controlField1));
		s.append(String.format("%02X", controlField2));
		for (byte b : sourceAddress) {
			s.append(String.format("%02X", b));
		}
		for (byte b : destinationAddress) {
			s.append(String.format("%02X", b));
		}
		s.append(String.format("%02X", payloadLength));
		for (byte b : tpciSecureApci) {
			s.append(String.format("%02X", b));
		}
		s.append(String.format("%02X", scf));
		for (byte b : seqNr) {
			s.append(String.format("%02X", b));
		}
		for (byte b : payload) {
			s.append(String.format("%02X", b));
		}
		for (byte b : mac) {
			s.append(String.format("%02X", b));
		}
		return s.toString();
	}

	/**
	 * Main-function to test the methods of this class.
	 */
	public static void main(String[] args) {
		String s = "2900BCE0110200010E03F11000273C45C9BF2B59E066BD0F";
		System.out.println("\t\t" + s);
		KnxTelegram k;
		try {
			k = new KnxTelegram(HexFormat.of().parseHex(s));
		} catch (KeystoreException e) {
			if (e.getReason() == KeystoreException.WRONG_LENGTH) {
				System.out.println("telegram must be at least 23 bytes long");
			}
			return;
		}
		System.out.println("toString():\t" + k);
		System.out.print("getBytes():\t");
		for (byte b : k.getBytes()) {
			System.out.printf("%02X", b);
		}
		System.out.println();
		System.out.println("getKeyId():\t" + k.getKeyId());
	}
}
