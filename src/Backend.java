package de.hu_berlin.keystore;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.OwnerPINBuilder;
import javacard.framework.OwnerPINxWithPredecrement;
import javacard.framework.SystemException;
import javacard.framework.Util;
import javacard.security.AESKey;
import javacard.security.CryptoException;
import javacard.security.KeyBuilder;
import javacard.security.RandomData;
import javacard.security.Signature;
import javacardx.crypto.Cipher;

public class Backend extends Applet {

	private static final byte INS_AUTHENTICATE = 0x20;
	private static final byte INS_GENERATE_WRAPPING_KEY = 0x10;
	private static final byte INS_REMOVE_WRAPPING_KEY = 0x30;
	private static final byte INS_WRAP_KEY = 0x40;
	private static final byte INS_UNWRAP_KEY = 0x50;
	private static final byte INS_DECRYPT = (byte) 0xA0;
	private static final byte INS_VERIFY = (byte) 0x80;
	private static final byte INS_DECRYPT_AND_VERIFY = (byte) 0xB0;

	private static final short SW_PIN_TYPE_NOT_IMPLEMENTED = 0x6901;
	private static final short SW_NO_SUCH_RND_ALGORITHM = 0x6902;
	private static final short SW_NO_SUCH_WKEY_ALGORITHM = 0x6903;
	private static final short SW_NO_SUCH_RKEY_ALGORITHM = 0x6904;
	private static final short SW_NO_SUCH_KEY_CIPHER_ALGORITHM = 0x6905;
	private static final short SW_NO_SUCH_MAC_ALGORITHM = 0x6906;
	private static final short SW_NO_SUCH_CIPHER_ALGORITHM = 0x6907;
	private static final short SW_OUT_OF_MEMORY = 0x6908;

	private static final short SW_EXECUTION_ERROR = 0x6400;

	private static OwnerPINxWithPredecrement pin;
	private static final byte TRY_LIMIT = 3;
	private static final byte MAX_PIN_SIZE = 6;
	private static final byte MIN_PIN_SIZE = 6;

	private static RandomData rng;
	private static AESKey wrappingEncrKey;
	private static AESKey wrappingAuthKey;
	private static Cipher keyCipher;
	private static Signature mac;
	private static AESKey key;
	private static Cipher cipher;

	/*
	 * Buffers used when executing INS_DECRYPT_AND_VERIFY
	 */
	private static byte iv[];
	private static byte tmp[];
	private static byte msg[];

	private static final short BLOCKSIZE = 16;
	private static final short WRAPPING_KEY_LEN = 32;
	private static final short KEY_LEN = 16;
	/*
	 * KEY_MAC_LEN must be a multiple of blocksize, because we do not use padding
	 * before encryption. To change that, change the encryption algorithm to one
	 * with padding.
	 */
	private static final short KEY_MAC_LEN = 16;
	private static final short WRAPPED_KEY_LEN = KEY_LEN + KEY_MAC_LEN;

	/*
	 * INSTALLATION RELATED FUNCTIONS
	 */

	private static short getPinOffset(byte[] bArray, short bOffset, byte bLength) {
		/* bOffset -> li i1 ... ili lc c1 ... clc la a1 ... ala */
		/* la: Länge der PIN, a1 ... ala: PIN */

		final byte li = bArray[bOffset];
		final byte lc = bArray[(short) (bOffset + li + 1)];
		return (short) (bOffset + li + lc + 3);
	}

	public static void install(byte[] bArray, short bOffset, byte bLength) {
		final short pinOff = getPinOffset(bArray, bOffset, bLength);
		final byte pinLen = bArray[(short) (pinOff - 1)];

		new Backend(bArray, pinOff, pinLen);
	}

	private static void initPin(byte[] buf, short off, byte len) {
		final byte pinType = OwnerPINBuilder.OWNER_PIN_X_WITH_PREDECREMENT;

		try {
			pin = (OwnerPINxWithPredecrement) OwnerPINBuilder.buildOwnerPIN(TRY_LIMIT, MAX_PIN_SIZE, pinType);
		} catch (SystemException e) {
			if (e.getReason() == SystemException.ILLEGAL_USE) {
				ISOException.throwIt(SW_PIN_TYPE_NOT_IMPLEMENTED);
			}
		}
		if (len == 0) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
		if (len < MIN_PIN_SIZE || len > MAX_PIN_SIZE) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}
		pin.update(buf, off, len);
	}

	private static void initRng() {
		try {
			rng = RandomData.getInstance(RandomData.ALG_KEYGENERATION);
		} catch (CryptoException e) {
			if (e.getReason() == CryptoException.NO_SUCH_ALGORITHM) {
				ISOException.throwIt(SW_NO_SUCH_RND_ALGORITHM);
			}
		}
	}

	private static void initWrappingKey() {
		final byte keyType = KeyBuilder.ALG_TYPE_AES;
		final byte memType = JCSystem.MEMORY_TYPE_PERSISTENT;
		final short keyLen = KeyBuilder.LENGTH_AES_256;
		final boolean keyEnc = false;

		try {
			wrappingEncrKey = (AESKey) KeyBuilder.buildKey(keyType, memType, keyLen, keyEnc);
			wrappingAuthKey = (AESKey) KeyBuilder.buildKey(keyType, memType, keyLen, keyEnc);
		} catch (CryptoException e) {
			if (e.getReason() == CryptoException.NO_SUCH_ALGORITHM) {
				ISOException.throwIt(SW_NO_SUCH_WKEY_ALGORITHM);
			}
		}
	}

	private static void initKeyCipher() {
		final byte algorithm = Cipher.CIPHER_AES_ECB;
		final byte padding = Cipher.PAD_NOPAD;
		final boolean extAccess = false;

		try {
			keyCipher = Cipher.getInstance(algorithm, padding, extAccess);
		} catch (CryptoException e) {
			if (e.getReason() == CryptoException.NO_SUCH_ALGORITHM) {
				ISOException.throwIt(SW_NO_SUCH_KEY_CIPHER_ALGORITHM);
			}
		}
	}

	private static void initMac() {
		final byte algorithm = Signature.ALG_AES_MAC_128_NOPAD;
		final boolean extAccess = false;

		try {
			mac = Signature.getInstance(algorithm, extAccess);
		} catch (CryptoException e) {
			if (e.getReason() == CryptoException.NO_SUCH_ALGORITHM) {
				ISOException.throwIt(SW_NO_SUCH_MAC_ALGORITHM);
			}
		}
	}

	private static void initKey() {
		final byte keyType = KeyBuilder.ALG_TYPE_AES;
		final byte memType = JCSystem.MEMORY_TYPE_TRANSIENT_DESELECT;
		final short keyLen = KeyBuilder.LENGTH_AES_128;
		final boolean keyEnc = false;

		try {
			key = (AESKey) KeyBuilder.buildKey(keyType, memType, keyLen, keyEnc);
		} catch (CryptoException e) {
			if (e.getReason() == CryptoException.NO_SUCH_ALGORITHM) {
				ISOException.throwIt(SW_NO_SUCH_RKEY_ALGORITHM);
			}
		}
	}

	private static void initCipher() {
		final byte algorithm = Cipher.ALG_AES_CTR;
		final boolean extAccess = false;

		try {
			cipher = Cipher.getInstance(algorithm, extAccess);
		} catch (CryptoException e) {
			if (e.getReason() == CryptoException.NO_SUCH_ALGORITHM) {
				ISOException.throwIt(SW_NO_SUCH_CIPHER_ALGORITHM);
			}
		}
	}

	private static void initBuffers() {
		final byte event = JCSystem.CLEAR_ON_DESELECT;
		try {
			iv = JCSystem.makeTransientByteArray(BLOCKSIZE, event);
			/*
			 * APDU-Buffer is 133 bytes long, that means 128 bytes of data, that means 106
			 * bytes of plaintext is possible. So we need (106 + 4) * 2 = 220 bytes here.
			 */
			tmp = JCSystem.makeTransientByteArray((short) 220, event);
			/*
			 * 106 bytes of plaintext means the message can be 16 * 8 bytes long.
			 */
			msg = JCSystem.makeTransientByteArray((short) (16 * 8), event);
		} catch (SystemException e) {
			if (e.getReason() == SystemException.NO_TRANSIENT_SPACE) {
				ISOException.throwIt(SW_OUT_OF_MEMORY);
			}
		}
	}

	protected Backend(byte[] bArray, short bOffset, byte bLength) {
		initPin(bArray, bOffset, bLength);
		initRng();
		initWrappingKey();
		initKeyCipher();
		initMac();
		initKey();
		initCipher();
		initBuffers();
		register();
	}

	/*
	 * FUNCTIONS USED WHEN PROCESSING AN APDU
	 */

	public void process(APDU apdu) {
		final byte buf[] = apdu.getBuffer();
		final byte ins = buf[ISO7816.OFFSET_INS];

		if (selectingApplet()) {
			return;
		}
		if (!apdu.isValidCLA()) {
			ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
		}
		switch (ins) {
		case INS_AUTHENTICATE:
			authenticate(apdu);
			return;
		case INS_GENERATE_WRAPPING_KEY:
			generateWrappingKey(apdu);
			return;
		case INS_REMOVE_WRAPPING_KEY:
			removeWrappingKey();
			return;
		case INS_WRAP_KEY:
			wrapKey(apdu);
			return;
		case INS_UNWRAP_KEY:
			unwrapKey(apdu);
			return;
		case INS_DECRYPT:
			decrypt(apdu);
			return;
		case INS_VERIFY:
			verify(apdu);
			return;
		case INS_DECRYPT_AND_VERIFY:
			final byte p1 = buf[ISO7816.OFFSET_P1];
			final boolean authOnly = p1 != 0;
			decryptAndVerify(apdu, authOnly);
			return;
		default:
			ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
		}
	}

	private static void authenticate(APDU apdu) {
		// apdu: 0 0x20 0 0 lc len pin_1 ... pin_len padding
		final byte buf[] = apdu.getBuffer();
		final short len = apdu.setIncomingAndReceive();
		final short lenOff = ISO7816.OFFSET_CDATA;
		final short pinOff = (short) (lenOff + 1);
		final byte pinLen = buf[lenOff];
		boolean isPinCorrect;

		if (len > MAX_PIN_SIZE + 1) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}
		if (isContactless()) {
			ISOException.throwIt(ISO7816.SW_AUTHENTICATION_METHOD_BLOCKED);
		}
		pin.decrementTriesRemaining();
		isPinCorrect = pin.check(buf, pinOff, pinLen);
		if (!isPinCorrect) {
			ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
		}
	}

	private static boolean isContactless() {
		final byte p = APDU.getProtocol();

		if (p == (APDU.PROTOCOL_MEDIA_CONTACTLESS_TYPE_A | APDU.PROTOCOL_T1)) {
			return true;
		}
		if (p == (APDU.PROTOCOL_MEDIA_CONTACTLESS_TYPE_B | APDU.PROTOCOL_T1)) {
			return true;
		}
		if (p == (APDU.PROTOCOL_MEDIA_CONTACTLESS_TYPE_F | APDU.PROTOCOL_T1)) {
			return true;
		}
		return false;
	}

	private static void generateWrappingKey(APDU apdu) {
		verifyPin();

		final byte buf[] = apdu.getBuffer();
		final short off = ISO7816.OFFSET_CDATA;
		final short len = (short) (WRAPPING_KEY_LEN * 2);
		final short off2 = (short) (off + WRAPPING_KEY_LEN);

		rng.nextBytes(buf, off, len);
		wrappingEncrKey.setKey(buf, off);
		wrappingAuthKey.setKey(buf, off2);
	}

	private static void removeWrappingKey() {
		verifyPin();
		wrappingEncrKey.clearKey();
		wrappingAuthKey.clearKey();
	}

	private static void verifyPin() {
		if (!pin.isValidated()) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
		pin.reset();
	}

	private static void wrapKey(APDU apdu) {
		final byte buf[] = apdu.getBuffer();
		final byte off = ISO7816.OFFSET_CDATA;
		final short len = apdu.setIncomingAndReceive();
		final short macOff = (short) (off + len);
		short ret;

		if (len != KEY_LEN) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}
		if (!isWrappingKeyInitialized()) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
		mac.init(wrappingAuthKey, Signature.MODE_SIGN);
		ret = mac.sign(buf, off, KEY_LEN, buf, macOff);
		if (ret != KEY_MAC_LEN) {
			ISOException.throwIt(SW_EXECUTION_ERROR);
		}
		keyCipher.init(wrappingEncrKey, Cipher.MODE_ENCRYPT);
		ret = keyCipher.doFinal(buf, off, WRAPPED_KEY_LEN, buf, off);
		if (ret != WRAPPED_KEY_LEN) {
			ISOException.throwIt(SW_EXECUTION_ERROR);
		}
		apdu.setOutgoingAndSend(off, WRAPPED_KEY_LEN);
	}

	private static void unwrapKey(APDU apdu) {
		final byte buf[] = apdu.getBuffer();
		final byte off = ISO7816.OFFSET_CDATA;
		final short len = apdu.setIncomingAndReceive();
		final short macOff = (short) (off + KEY_LEN);
		short ret;
		boolean isValid;

		if (len != WRAPPED_KEY_LEN) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}
		if (!isWrappingKeyInitialized()) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
		keyCipher.init(wrappingEncrKey, Cipher.MODE_DECRYPT);
		ret = keyCipher.doFinal(buf, off, WRAPPED_KEY_LEN, buf, off);
		if (ret != WRAPPED_KEY_LEN) {
			ISOException.throwIt(SW_EXECUTION_ERROR);
		}
		mac.init(wrappingAuthKey, Signature.MODE_VERIFY);
		isValid = mac.verify(buf, off, KEY_LEN, buf, macOff, KEY_MAC_LEN);
		if (!isValid) {
			ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
		}
		key.setKey(buf, off);
	}

	private static boolean isWrappingKeyInitialized() {
		if (!wrappingEncrKey.isInitialized()) {
			return false;
		}
		if (!wrappingAuthKey.isInitialized()) {
			return false;
		}
		return true;
	}

	private static void decrypt(APDU apdu) {
		final byte buf[] = apdu.getBuffer();
		final byte ivOff = ISO7816.OFFSET_CDATA;
		final short len = apdu.setIncomingAndReceive();
		final short ivLen = BLOCKSIZE;
		final short cipherOff = (short) (ivOff + ivLen);
		final short cipherLen = (short) (len - ivLen);
		final short plainOff = (short) (ivOff + len);
		short plainLen;

		if (len <= ivLen) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}
		if (!key.isInitialized()) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
		cipher.init(key, Cipher.MODE_DECRYPT, buf, ivOff, ivLen);
		plainLen = cipher.doFinal(buf, cipherOff, cipherLen, buf, plainOff);
		if (plainLen != cipherLen) {
			ISOException.throwIt(SW_EXECUTION_ERROR);
		}
		apdu.setOutgoingAndSend(plainOff, plainLen);
	}

	private static void verify(APDU apdu) {
		final byte buf[] = apdu.getBuffer();
		final byte macOff = ISO7816.OFFSET_CDATA;
		final short len = apdu.setIncomingAndReceive();
		final short macLen = 4;
		final short msgOff = (short) (macOff + macLen);
		final short msgLen = (short) (len - macLen);
		short ret;
		boolean isValid;

		if (len <= macLen) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}
		if (!key.isInitialized()) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
		mac.init(key, Signature.MODE_SIGN);
		ret = mac.sign(buf, msgOff, msgLen, buf, msgOff);
		if (ret < macLen) {
			ISOException.throwIt(SW_EXECUTION_ERROR);
		}
		isValid = (0 == Util.arrayCompare(buf, macOff, buf, msgOff, macLen));
		if (!isValid) {
			ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
		}
	}

	private static void decryptAndVerify(APDU apdu, boolean authOnly) {
		final byte buf[] = apdu.getBuffer();
		final short len = apdu.setIncomingAndReceive();
		final short macLen = 4;
		final byte dataLen = (byte) (len - 18 - macLen);
		final short cipherLen = (short) (authOnly ? macLen : dataLen + macLen);
		final short plainOff = cipherLen;
		short plainLen;
		final short msgLen = (short) (16 * (2 + ((dataLen + 2) / 16)));
		short ret;
		boolean isValid;

		if (len <= 0) {
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}
		if (!key.isInitialized()) {
			ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
		}
		// build iv
		Util.arrayFillNonAtomic(iv, (short) 0, (short) iv.length, (byte) 0);
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 12), iv, (short) 0, (short) 6);
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 4), iv, (short) 6, (short) 2);
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 6), iv, (short) 8, (short) 2);
		iv[14] = (byte) 1;
		// get ciphertext
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + len - macLen), tmp, (short) 0, macLen);
		if (!authOnly) {
			Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 18), tmp, macLen, dataLen);
		}
		// decrypt
		cipher.init(key, Cipher.MODE_DECRYPT, iv, (short) 0, (short) iv.length);
		plainLen = cipher.doFinal(tmp, (short) 0, cipherLen, tmp, plainOff);
		if (plainLen != cipherLen) {
			ISOException.throwIt(SW_EXECUTION_ERROR);
		}
		// build message
		Util.arrayFillNonAtomic(msg, (short) 0, (short) msg.length, (byte) 0);
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 12), msg, (short) 0, (short) 6);
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 4), msg, (short) 6, (short) 2);
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 6), msg, (short) 8, (short) 2);
		msg[11] = (byte) (buf[ISO7816.OFFSET_CDATA + 3] & 0b1000_0000);
		Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 9), msg, (short) 12, (short) 2);
		msg[15] = dataLen;
		msg[16 + 1] = 1;
		msg[16 + 2] = buf[ISO7816.OFFSET_CDATA + 11];
		if (authOnly) {
			Util.arrayCopyNonAtomic(buf, (short) (ISO7816.OFFSET_CDATA + 18), msg, (short) 19, dataLen);
		} else {
			Util.arrayCopyNonAtomic(tmp, (short) (plainOff + macLen), msg, (short) 19, dataLen);
		}
		// verify
		mac.init(key, Signature.MODE_SIGN);
		ret = mac.sign(msg, (short) 0, msgLen, iv, (short) 0);
		if (ret < macLen) {
			ISOException.throwIt(SW_EXECUTION_ERROR);
		}
		isValid = (0 == Util.arrayCompare(iv, (short) 0, tmp, plainOff, macLen));
		if (!isValid) {
			ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
		}
		if (!authOnly) {
			Util.arrayCopyNonAtomic(tmp, (short) (plainOff + macLen), buf, (short) 0, dataLen);
			apdu.setOutgoingAndSend((short) 0, dataLen);
		}
	}
}
