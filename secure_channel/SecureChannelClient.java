import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

public class SecureChannelClient {

	private static Card card;
	private static CardChannel channel;

	private static void connect() throws CardException {
		// show the list of available terminals
		TerminalFactory factory = TerminalFactory.getDefault();
		List<CardTerminal> terminals = factory.terminals().list();
		// get the first terminal
		CardTerminal terminal = terminals.get(0);
		// establish a connection with the card
		card = terminal.connect("*");
		channel = card.getBasicChannel();
	}

	private static void disconnect() throws CardException {
		card.disconnect(false);
	}

	private static void selectApplet() throws Exception {
		byte[] aid = new byte[] {1, 2, 3, 4, 5, 0x25};
		CommandAPDU apdu = new CommandAPDU(0, (byte) 0xA4, 4, 0, aid);
		ResponseAPDU response = channel.transmit(apdu);
		if (response.getSW() != 0x9000) {
			throw new Exception("Select applet failed.");
		}
	}

	private static void mutualAuthentication(byte securityLevel) throws Exception {
		byte[] key = new byte[] {
			0x40, 0x41, 0x42, 0x43,
			0x44, 0x45, 0x46, 0x47,
			0x48, 0x49, 0x4a, 0x4b,
			0x4c, 0x4d, 0x4e, 0x4f,
		};
		byte[] hostChallenge = getHostChallenge();
		InitializeUpdateResponse response = initializeUpdate(hostChallenge);
		if (response.scpIdentifier != 3) {
			throw new Exception("Wrong SCP Version: Must use SCP03.");
		}
		byte[] sEnc = derive(key, "S-ENC", hostChallenge, response.cardChallenge);
		byte[] sMac = derive(key, "S-MAC", hostChallenge, response.cardChallenge);
		byte[] cardCryptogram = derive(sMac, "card-cryptogram", hostChallenge, response.cardChallenge);
		if (!Arrays.equals(cardCryptogram, response.cardCryptogram)) {
			throw new Exception("Verification of the card cryptogram failed.");
		}
		byte[] hostCryptogram = derive(sMac, "host-cryptogram", hostChallenge, response.cardChallenge);
		byte[] mac = getMacForExternalAuthenticate(hostCryptogram, sMac, securityLevel);
		externalAuthenticate(hostCryptogram, mac, securityLevel);
	}

	private static void externalAuthenticate(byte[] hostCryptogram, byte[] mac, byte securityLevel) throws Exception {
		byte[] data = Arrays.copyOf(hostCryptogram, hostCryptogram.length + mac.length);
		System.arraycopy(mac, 0, data, hostCryptogram.length, mac.length);
		CommandAPDU apdu = new CommandAPDU(0x84, 0x82, securityLevel, 0, data, 16);
		ResponseAPDU response = channel.transmit(apdu);
		if (response.getSW() != 0x9000){
			throw new Exception(response.toString());
		}
	}

	private static byte[] getMacForExternalAuthenticate(byte[] hostCryptogram, byte[] key, byte securityLevel) throws Exception {
		byte[] macInput = new byte[29];
		macInput[16] = (byte) 0x84;
		macInput[17] = (byte) 0x82;
		macInput[18] = securityLevel;
		macInput[19] = 0;
		macInput[20] = 16;
		System.arraycopy(hostCryptogram, 0, macInput, 21, hostCryptogram.length);
		byte[] mac = CMAC.cmac(macInput, key);
		return Arrays.copyOf(mac, 8);
	}

	private static byte[] derive(byte[] key, String type, byte[] hostChallenge, byte[] cardChallenge) throws Exception {
		byte derivationConstant;
		switch (type) {
			case "card-cryptogram": derivationConstant = 0; break;
			case "host-cryptogram": derivationConstant = 1; break;
			case "S-ENC":           derivationConstant = 4; break;
			case "S-MAC":           derivationConstant = 6; break;
			case "S-RMAC":          derivationConstant = 7; break;
			default:                throw new Exception("Invalid type.");
		}
		byte[] data = new byte[32];
		data[11] = derivationConstant;
		data[12] = 0;
		data[13] = 0;
		if (type.equals("card-cryptogram") || type.equals("host-cryptogram")) {
			data[14] = 0x40;
		} else {
			data[14] = (byte) 0x80;
		}
		data[15] = 1;
		System.arraycopy(hostChallenge, 0, data, 16, hostChallenge.length);
		System.arraycopy(cardChallenge, 0, data, 24, cardChallenge.length);
		byte[] mac = CMAC.cmac(data, key);
		if (type.equals("card-cryptogram") || type.equals("host-cryptogram")) {
			byte[] result = new byte[8];
			System.arraycopy(mac, 0, result, 0, result.length);
			return result;
		}
		return mac;
	}

	private static InitializeUpdateResponse initializeUpdate(byte[] hostChallenge) throws Exception {
		CommandAPDU apdu = new CommandAPDU(0x80, 0x50, 0, 0, hostChallenge, 0);
		ResponseAPDU response = channel.transmit(apdu);
		if (response.getSW() != 0x9000){
			throw new Exception(response.toString());
		}
		return new InitializeUpdateResponse(response.getData());
	}

	private static byte getSecurityLevel() throws Exception {
		CommandAPDU apdu = new CommandAPDU(0, 0x10, 0, 0);
		ResponseAPDU response = channel.transmit(apdu);
		if (response.getSW() != 0x9000){
			throw new Exception(response.toString());
		}
		byte[] data = response.getData();
		if (data.length != 1) {
			throw new Exception("Unexpected response data length.");
		}
		return data[0];
	}

	/*
	 * TODO: remember used challenges (challenges should be unique to this session)
	 */
	private static byte[] getHostChallenge() {
		byte[] b = new byte[8];
		Random r = new Random();
		r.nextBytes(b);
		return b;
	}

	private static void printApdu(CommandAPDU apdu) {
		System.out.println(apdu);
		byte[] data = apdu.getData();
		for (byte b : data) {
			System.out.printf("%02x ", b);
		}
		System.out.println();
	}

	private static void printApdu(ResponseAPDU apdu) {
		System.out.println(apdu);
		byte[] data = apdu.getData();
		for (byte b : data) {
			System.out.printf("%02x ", b);
		}
		System.out.println();
	}

	private static void sendAndPrintResponse(CommandAPDU apdu) throws CardException {
		printApdu(apdu);
		ResponseAPDU res = channel.transmit(apdu);
		printApdu(res);
	}

	public static void main(String[] args) throws Exception {
		connect();
		selectApplet();

		byte securityLevel = 1;
		mutualAuthentication(securityLevel);
		System.out.printf("Security level: %02x\n", getSecurityLevel());

		CommandAPDU apdu;

		System.out.println();
		apdu = new CommandAPDU((byte)0x00, 0, 0, 0, new byte[] {1,2,3});
		sendAndPrintResponse(apdu);

		System.out.println();
		apdu = new CommandAPDU((byte)0x80, 0, 0, 0, new byte[] {1,2,3});
		sendAndPrintResponse(apdu);

		System.out.println();
		apdu = new CommandAPDU((byte)0x84, 0, 0, 0, new byte[] {1,2,3});
		sendAndPrintResponse(apdu);

		disconnect();
	}
}
