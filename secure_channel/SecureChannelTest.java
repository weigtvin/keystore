package secureChannelTest;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import org.globalplatform.SecureChannel;
import org.globalplatform.GPSystem;

public class SecureChannelTest extends Applet {

	public static void install(byte[] bArray, short bOffset, byte bLength) {
		new SecureChannelTest();
	}

	protected SecureChannelTest() {
		register();
	}

	public void process(APDU apdu) {
		if (selectingApplet()) {
			return;
		}
		final byte[] buf = apdu.getBuffer();
		SecureChannel sc = GPSystem.getSecureChannel();
		if (isSecurityRelated(apdu)) {
			final short len = sc.processSecurity(apdu);
			if (len != 0) {
				apdu.setOutgoingAndSend(ISO7816.OFFSET_CDATA, len);
			}
			return;
		}
		byte ins = buf[ISO7816.OFFSET_INS];
		if (ins == 0x10) {
			final byte securityLevel = sc.getSecurityLevel();
			buf[ISO7816.OFFSET_CDATA] = securityLevel;
			apdu.setOutgoingAndSend(ISO7816.OFFSET_CDATA, (short) 1);
			return;
		}
		short len = apdu.setIncomingAndReceive();
		apdu.setOutgoingAndSend((short)0, (short)(len + 5));
	}

	public void deselect() {
		SecureChannel sc = GPSystem.getSecureChannel();
		sc.resetSecurity();
	}

	private static boolean isSecurityRelated(APDU apdu) {
		final byte[] buf = apdu.getBuffer();
		final byte cla = buf[ISO7816.OFFSET_CLA];
		final byte ins = buf[ISO7816.OFFSET_INS];

		if (isInitializeUpdateCommand(cla, ins)) {
			return true;
		}
		if (isExternalAuthenticateCommand(cla, ins)) {
			return true;
		}
		return false;
	}

	private static boolean isInitializeUpdateCommand(byte cla, byte ins) {
		return (cla == (byte) 0x80 && ins == 0x50);
	}

	private static boolean isExternalAuthenticateCommand(byte cla, byte ins) {
		return (cla == (byte) 0x84 && ins == (byte) 0x82);
	}
}
