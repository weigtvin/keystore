import java.util.HexFormat;

/*
 * This class is used for the response data of the "INITIALIZE UPDATE" command.
 * See GlobalPlatform Secure Channel Protocol '03',
 * Card Specification v2.3 - Amendment D Version 1.1.2
 * (https://globalplatform.org/wp-content/uploads/2014/07/GPC_2.3_D_SCP03_v1.1.2_PublicRelease.pdf)
 */
public final class InitializeUpdateResponse {

	public final byte[] keyDiversificationData;
	public final byte   keyVersionNumber;
	public final byte   scpIdentifier;
	public final byte   scpIParameter;
	public final byte[] cardChallenge;
	public final byte[] cardCryptogram;
	public final byte[] sequenceCounter;

	public InitializeUpdateResponse(byte[] b) throws Exception {
		if (b.length != 32) {
			throw new Exception("Wrong Length: Length must be 32 Bytes.");
		}
		keyDiversificationData = new byte[10];
		System.arraycopy(b, 0, keyDiversificationData, 0, keyDiversificationData.length);

		keyVersionNumber = b[10];
		scpIdentifier = b[11];
		scpIParameter = b[12];

		cardChallenge = new byte[8];
		System.arraycopy(b, 13, cardChallenge, 0, cardChallenge.length);

		cardCryptogram = new byte[8];
		System.arraycopy(b, 21, cardCryptogram, 0, cardCryptogram.length);

		sequenceCounter = new byte[3];
		System.arraycopy(b, 29, sequenceCounter, 0, sequenceCounter.length);
	}

	public String toString() {
		StringBuilder s = new StringBuilder();

		s.append("Key diversification data:\t");
		for (byte b : keyDiversificationData) {
			s.append(String.format("%02X", b));
		}
		s.append("\n");

		s.append(String.format("Key version number:\t\t%02X\n", keyVersionNumber));
		s.append(String.format("SCP identifier:\t\t\t%02X\n", scpIdentifier));
		s.append(String.format("SCP 'i'-parameter:\t\t%02X\n", scpIParameter));

		s.append("Card challenge:\t\t\t");
		for (byte b : cardChallenge) {
			s.append(String.format("%02X", b));
		}
		s.append("\n");

		s.append("Card cryptogram:\t\t");
		for (byte b : cardCryptogram) {
			s.append(String.format("%02X", b));
		}
		s.append("\n");

		s.append("Sequence Counter:\t\t");
		for (byte b : sequenceCounter) {
			s.append(String.format("%02X", b));
		}

		return s.toString();
	}

	public static void main(String[] args) throws Exception {
		byte[] b = HexFormat.of().parseHex("0000013627367099538201037063B6DE8B616AA1ABFE7CA455948FB1F3000154");
		InitializeUpdateResponse res = new InitializeUpdateResponse(b);
		System.out.println(res);
	}
}
