import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/*
 * NIST Special Publication 800-38B
 * Recommendation for Block Cipher Modes of Operation:
 * The CMAC Mode for Authentication
 * (https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-38b.pdf)
 */
public class CMAC {

	private static final int BLOCKSIZE = 16;

	private static byte[] encrypt(byte[] plaintext, byte[] key) throws Exception {
		Cipher c = Cipher.getInstance("AES/ECB/NoPadding");
                Key k = new SecretKeySpec(key, "AES");
                c.init(Cipher.ENCRYPT_MODE, k);
                return c.doFinal(plaintext);
	}

	private static byte[][] generateSubkeys(byte[] key) throws Exception {
		if (key.length != 16) {
			throw new Exception("invalid key size");
		}
		byte[][] subkey = new byte[2][16];
		byte[] L = encrypt(new byte[BLOCKSIZE], key);
		subkey[0] = shift(L);
		if (msb(L) != 0) {
			xorRb(subkey[0]);
		}
		subkey[1] = shift(subkey[0]);
		if (msb(subkey[0]) != 0) {
			xorRb(subkey[1]);
		}
		return subkey;
	}

	public static byte[] cmac(byte[] input, byte[] key) throws Exception {
		if (input.length == 0) {
			throw new Exception("Cannot calculate MAC on empty input.");
		}
		assert (input.length > 0);

		byte[][] subkey = generateSubkeys(key);
		input = xorSubkey(input, subkey);
		assert (input.length % BLOCKSIZE == 0);
		byte[][] data = to2dArray(input);
		int numBlocks = data.length;
		byte[][] c = new byte[numBlocks + 1][BLOCKSIZE];
		for (int i = 1; i <= numBlocks; ++i) {
			c[i] = encrypt(xor(c[i - 1], data[i - 1]), key);
		}
		return c[numBlocks];
	}

	private static byte[][] to2dArray(byte[] input) {
		assert (input.length % BLOCKSIZE == 0);

		int numBlocks = input.length / BLOCKSIZE;
		byte[][] res = new byte[numBlocks][BLOCKSIZE];
		for (int i = 0; i < numBlocks; ++i) {
			for (int j = 0; j < BLOCKSIZE; ++j) {
				res[i][j] = input[i * BLOCKSIZE + j];
			}
		}
		return res;
	}

	private static int getNumberOfBlocks(int len) {
		assert (len > 0);
		return ((len - 1) / BLOCKSIZE) + 1;
	}

	private static byte[] xor(byte[] b1, byte[] b2) {
		assert (b1.length == BLOCKSIZE);
		assert (b2.length == BLOCKSIZE);

		byte[] res = new byte[BLOCKSIZE];
		for (int i = 0; i < BLOCKSIZE; ++i) {
			res[i] = (byte) (b1[i] ^ b2[i]);
		}
		return res;
	}

	private static byte[] xorSubkey(byte[] b, byte[][] subkey) {
		int numBlocks = getNumberOfBlocks(b.length);
		byte[] res = new byte[BLOCKSIZE * numBlocks];
		assert (b.length <= res.length);
		System.arraycopy(b, 0, res, 0, b.length);
		int index = 0;
		if (b.length < res.length) {
			assert (res.length - b.length < BLOCKSIZE);
			res[b.length] = (byte) 0b1000_0000;
			index = 1;
		}
		for (int i = 0; i < BLOCKSIZE; ++i) {
			res[res.length - BLOCKSIZE + i] ^= subkey[index][i];
		}
		return res;
	}

	private static void xorRb(byte[] b) {
		assert (b.length == BLOCKSIZE);
		b[BLOCKSIZE - 1] ^= 0b1000_0111;
	}

	private static int msb(byte[] b) {
		return msb(b[0]);
	}

	private static int msb(byte b) {
		return b >= 0 ? 0 : 1;
	}

	private static byte[] shift(byte[] b) {
		byte[] res = new byte[b.length];

		res[0] = (byte) (b[0] << 1);
		for (int i = 1; i < b.length; ++i) {
			res[i - 1] += msb(b[i]);
			res[i] = (byte) (b[i] << 1);
		}

		return res;
	}

	private static void printByteArray(byte[] b) {
		for (byte by : b) {
			System.out.printf("%02x", by);
		}
		System.out.println();
	}

	private static void printByteBinary(byte b) {
		for (int i = 0; i < 8; ++i) {
			System.out.print(msb((byte) (b << i)));
		}
	}

	private static void printByteArrayBinary(byte[] b) {
		for (byte by : b) {
			printByteBinary(by);
		}
		System.out.println();
	}

	public static void main(String[] args) throws Exception {
		byte[] key = new byte[] {
			0x40, 0x41, 0x42, 0x43,
			0x44, 0x45, 0x46, 0x47,
			0x48, 0x49, 0x4a, 0x4b,
			0x4c, 0x4d, 0x4e, 0x4f,
		};

		/* Block-aligned data */
		byte[] data = new byte[] {
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x04,
			0x00, 0x00, (byte)0x80, 0x01,

			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x78, 0x38, 0x4f, 0x47,
			(byte)0xc3, 0x2f, (byte)0x99, 0x5d,
		};
		byte[] result = cmac(data, key);
		printByteArray(result);
		System.out.println("bf6bf7daebb46d235c95de6cfef56dba expected");

		/* Non block-aligned data */
		byte[] data2 = new byte[] {
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x04,
			0x00, 0x00, (byte)0x80, 0x01,

			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x78, 0x38, 0x4f, 0x47,
		};
		byte[] result2 = cmac(data2, key);
		printByteArray(result2);
		System.out.println("e5fd33d0c6573e797b97de89093b8fe4 expected");
	}
}
