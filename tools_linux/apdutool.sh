#!/bin/sh

if [ -z "$JC_HOME_TOOLS" ]
then
	echo "JC_HOME_TOOLS not set."
	exit 1
fi

if [ -z "$JC_HOME_SIMULATOR" ]
then
	echo "JC_HOME_SIMULATOR not set."
	exit 1
fi

classpath=""

for jar in \
	asm-8.0.1.jar \
	commons-cli-1.4.jar \
	commons-logging-1.2-9f99a00.jar \
	json.jar \
	tools.jar
do
	classpath="$classpath:$JC_HOME_TOOLS/lib/$jar"
done

for jar in \
	jctasks_simulator.jar \
	tools_simulator.jar \
	api_classic.jar \
	api_classic_annotations.jar
do
	classpath="$classpath:$JC_HOME_SIMULATOR/lib/$jar"
done

class="com.sun.javacard.apdutool.Main"

java -Djc.home="$homedir" -classpath "$classpath" "$class" "$@"
