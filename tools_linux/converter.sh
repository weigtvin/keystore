#!/bin/sh

if [ -z "$JC_HOME_TOOLS" ]
then
	echo "JC_HOME_TOOLS not set."
	exit 1
fi

classpath=""

for jar in \
	asm-8.0.1.jar \
	commons-cli-1.4.jar \
	commons-logging-1.2-9f99a00.jar \
	jctasks_tools.jar \
	json.jar \
	tools.jar \
	api_classic-3.1.0.jar \
	api_classic_annotations-3.1.0.jar
do
	classpath="$classpath:$JC_HOME_TOOLS/lib/$jar"
done

class="com.sun.javacard.converter.Main"

java -Djc.home="$homedir" -classpath "$classpath" "$class" "$@"
