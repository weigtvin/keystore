#! /usr/bin/env python3
# TODO check password / signature
# TODO key_ids ermitteln (wenn möglich) und dict aus key_id und key zurückgeben

import sys
import xml.etree.ElementTree as ET
from hashlib import pbkdf2_hmac
from hashlib import sha256
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
import base64

def parse(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    date = root.get('Created')
    keys = []
    for group in root.iter('{http://knx.org/xml/keyring/1}Group'):
        keys.append(group.get('Key'))
    return date, keys

def get_key(password):
    return pbkdf2_hmac('sha256', bytes(password, 'utf-8'), b'1.keyring.ets.knx.org', 65_536, dklen=16)

def get_iv(date):
    return sha256(bytes(date, 'utf-8')).digest()[:16]

def decrypt(ciphertext, key, iv):
    cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
    decryptor = cipher.decryptor()
    plaintext = decryptor.update(ciphertext) + decryptor.finalize()
    return plaintext

def get_keys_from_knx_keyring(filename, password):
    res = []
    date, keys = parse(filename)
    key = get_key(password)
    iv = get_iv(date)
    for k in keys:
        ciphertext = base64.b64decode(k)
        plaintext = decrypt(ciphertext, key, iv)
        res.append(plaintext)
    return res

if (len(sys.argv) != 3):
    print("Extrahiert KNX-Laufzeitschlüssel aus einer Schlüsselbund-Datei.")
    print(f"Usage: {sys.argv[0]} FILE PASSWORD")
    print("FILE ist die Schlüsselbund-Datei (.knxkeys)")
    print("PASSWORD wurde in der ETS beim Export des Schlüsselbunds gesetzt.")
    exit()

filename = sys.argv[1]
password = sys.argv[2]
keys = get_keys_from_knx_keyring(filename, password)
for key in keys:
    print(key.hex())
