Dieses Repository enthält Programme,
die im Rahmen der Bachelorarbeit "Sicherer KNX-Schlüsselspeicher auf einer Javacard" am Lehrstuhl Systemarchitektur entstanden sind.

Im Verzeichnis `src` befinden sich die Quelldateien für den Schlüsselspeicher.
Kompilieren und Ausführen:

	cd src/
	javac de/hu_berlin/keystore/Keystore.java
	java de.hu_berlin.keystore.Keystore

Im Verzeichnis `bin` befindet sich das kompilierte Javacard-Applet,
welches sich auf einer Javacard installieren lässt.
Der dazugehörige Quellcode befindet sich in `src/Backend.java`.
